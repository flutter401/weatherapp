import 'package:flutter/material.dart';
import '';

void main() {
  runApp(const AlignExample());
}

class AlignExample extends StatelessWidget{
  const AlignExample({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context){
    return MaterialApp(
    debugShowCheckedModeBanner:false ,
  home: Scaffold(
    body: Center(
      child: SizedBox(
        width: 600.0,
        child: AspectRatio(
          aspectRatio: 1.5,
          child: Container(
            color: Colors.lightGreenAccent,
            child: const FlutterLogo(),
          ),
        ),
      ),
    ),
  ),
/*
 home: Scaffold(
        body: Center(
          child: Container(
            height: 200.0,
            width: 600.0,
            color: Colors.red,
            child: Align(
              //alignment: Alignment(-0.5,-0.4),
              alignment: Alignment.center,
              child:Container(
                color: Colors.cyan,
                child: const Text('Align !!!!'),
              )
            )
          ),
        ),
      ),
*/
    );
  }
}


